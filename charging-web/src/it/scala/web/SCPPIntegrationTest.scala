package web

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpEntity, _}
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import core.{SCPPMessage, Tariff}
import org.joda.time
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.{PatienceConfiguration, ScalaFutures}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Matchers, WordSpecLike}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}
import scala.language.postfixOps

/** Integration test - can be run manually or with sbt
  *
  * Default values
  *
  * {{{
  * host = localhost
  * port = 8080
  * }}}
  *
  * can be set with
  *
  * {{{
  *   -Dit.host=localhost -Dit.port=8080
  * }}}
  */
class SCPPIntegrationTest(_system: ActorSystem) extends TestKit(_system) with ImplicitSender with SCPPProtocol
  with WordSpecLike with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with MockFactory {

  implicit val materializer = ActorMaterializer()
  implicit val ec = ExecutionContext.Implicits.global
  implicit val timeout: PatienceConfiguration.Timeout = Timeout(5 seconds)

  private val host = sys.props.get("it.host").getOrElse("localhost")
  private val port = sys.props.get("it.port").getOrElse("8080").toInt

  private val scppUrl = s"http://$host:$port/scpp"
  private val tariffUrl = s"http://$host:$port/tariff"

  def this() = this(ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  private def scpp[U](message: SCPPMessage)(fun: HttpResponse => U) = {
    call(scppUrl, POST, Some(serialize(message))) { response =>
      fun(response)
    }
  }

  private def tariff[U](tariff: Tariff)(fun: HttpResponse => U) = {
    call(tariffUrl, POST, Some(serialize(tariff))) { response =>
      fun(response)
    }
  }

  private def serialize(value: SCPPMessage) =
    Await.result(Marshal(value).to[RequestEntity], timeout.value)

  private def serialize(value: Tariff) =
    Await.result(Marshal(value).to[RequestEntity], timeout.value)

  private def call[U](url: String, method: HttpMethod, entity: Option[RequestEntity] = None)(fun: HttpResponse => U)(implicit timeout: PatienceConfiguration.Timeout) = {
    ScalaFutures.whenReady(Http().singleRequest(
      HttpRequest(uri = url, method = method, entity = entity.getOrElse(HttpEntity.Empty))), timeout = timeout) { response =>
      system.log.info(s"Calling ${method.value} for $url ends with ${response.status}")
      fun(response)
    }
  }

  "Client" must {

    "receive BadRequest when POSTing SCPP without tariff" in {
      scpp(SCPPMessage("aaa", time.DateTime.now, time.DateTime.now, 10)) { response =>
        response.status shouldBe BadRequest
      }
    }

    "successfully send Tariff and messages" in {
      val t = time.DateTime.now
      tariff(Tariff(1, 2, 3, t.plusDays(1))) { response =>
        response.status shouldBe Created
      }
      scpp(SCPPMessage("aaa", t.minusDays(10), t, 10)) { response =>
        response.status shouldBe BadRequest
      }
      scpp(SCPPMessage("aaa", t.plusDays(5), t.plusDays(10), 10)) { response =>
        response.status shouldBe OK
      }
      scpp(SCPPMessage("aaa", t.plusDays(6), t.plusDays(10), 20)) { response =>
        response.status shouldBe OK
      }
      scpp(SCPPMessage("aaa", t.plusDays(7), t.plusDays(10), 30)) { response =>
        response.status shouldBe OK
      }
      call(scppUrl, GET) { response =>
        response.status shouldBe OK
      }
    }
  }

}