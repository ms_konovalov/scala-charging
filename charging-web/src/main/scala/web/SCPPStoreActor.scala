package web

import akka.actor._
import core.{FeeCalculator, NoSuitableTariffFound, SCPPMessage, Tariff}
import org.joda.time.DateTime

import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}

object SCPPStoreActor {
  def prop(feeCalculator: FeeCalculator, clock: Clock = new Clock(){}): Props = Props(classOf[SCPPStoreActor], feeCalculator, clock)
}

/** Actor that stores all transactions info and calculated fees via [[feeCalculator]]
  *
  * @param feeCalculator fee calculator
  * @param clock clock to ease unit testing
  */
class SCPPStoreActor(feeCalculator: FeeCalculator, clock: Clock = new Clock(){}) extends Actor with ActorLogging {

  private val scppStore: ListBuffer[SCPPMessage] = ListBuffer()
  private val tariffStore: ListBuffer[Tariff] = ListBuffer()

  override def receive: Receive = {

    case StoreCommand(message) =>
      Try(feeCalculator.calculate(message, tariffStore.toList)) match {
        case Success(calculated) =>
          scppStore += calculated
          sender() ! Status.Success(calculated)
        case Failure(NoSuitableTariffFound) =>
          sender() ! Status.Failure(NoSuitableTariffFound)
        case Failure(ex) =>
          sender() ! Status.Failure(ex)
      }

    case GetCommand(customer, year, month) =>
      sender() ! GetResult(scppStore.filter(m =>
        m.customerId.equals(customer) && m.startTime.year().get() == year && m.startTime.monthOfYear().get() == month))

    case GetAllCommand =>
      sender() ! GetResult(scppStore.toList)

    case AddTariffCommand(tariff) =>
      if (getAvailableTime.isBefore(tariff.activeStarting)) {
        tariffStore += tariff
        sender() ! Status.Success(tariff)
      } else {
        sender() ! Status.Failure(WrongTariffStartDate)
      }
  }

  private def getAvailableTime = {
    val now = clock.getCurrentDateTime
    if (tariffStore.isEmpty || tariffStore.last.activeStarting.isBefore(now)) {
      now
    } else {
      tariffStore.last.activeStarting
    }
  }
}

/** Command to calculate and store new [[SCPPMessage]]
  *
  * @param message transaction message
  */
case class StoreCommand(message: SCPPMessage)

/** Command to return all stored [[SCPPMessage]]s */
case object GetAllCommand

/** Command to get transactions for customer for the month to make invoice
  *
  * @param customer customer id
  * @param year year
  * @param month month number
  */
case class GetCommand(customer: String, year: Int, month: Int)

/** Result of operations for [[GetCommand]] and [[GetAllCommand]]
  *
  * @param items transactions
  */
case class GetResult(items: Seq[SCPPMessage])

/** Command to add tariff
  *
  * @param tariff tariff
  */
case class AddTariffCommand(tariff: Tariff)

/** Error if tariff is invalid */
case object WrongTariffStartDate extends Exception

/** Interface to ease unit testing */
trait Clock {

  def getCurrentDateTime = DateTime.now()
}

