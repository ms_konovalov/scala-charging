package web

import java.io.ByteArrayOutputStream
import java.time.YearMonth
import java.time.format.TextStyle
import java.util.Locale

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.marshalling.ToResponseMarshallable.apply
import akka.http.scaladsl.model.HttpCharsets._
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.ContentDispositionTypes._
import akka.http.scaladsl.model.headers.`Content-Disposition`
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.ExceptionHandler
import akka.pattern.{AskTimeoutException, ask}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.github.tototoshi.csv.CSVWriter
import core.{FeeCalculator, NoSuitableTariffFound, SCPPMessage, Tariff}
import org.joda.time.format.ISODateTimeFormat

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.io.StdIn
import scala.language.postfixOps

class SCPPRoute(scppStoreActor: ActorRef, clock: Clock = new Clock(){})
               (implicit am: ActorMaterializer, ec: ExecutionContext)
  extends SCPPProtocol {

  import akkahttptwirl.TwirlSupport._

  implicit val timeout = Timeout(5 seconds)

  val exceptionHandler = ExceptionHandler {
    case WrongTariffStartDate =>
      convertError(BadRequest, ErrorResponse("Wrong tariff start date"))
    case NoSuitableTariffFound =>
      convertError(BadRequest, ErrorResponse("No suitable tariff found to process this message"))
    case e: AskTimeoutException =>
      convertError(InternalServerError, ErrorResponse("Operation timeout"))
    case e =>
      e.printStackTrace()
      convertError(InternalServerError, ErrorResponse("Operation failed"))
  }

  val route =
    handleExceptions(exceptionHandler) {
      pathPrefix("scpp") {
        pathEnd {
          post {
            entity(as[SCPPMessage]) { data =>
              logRequest("STORE-SCPP") {
                onComplete(scppStoreActor ? StoreCommand(data)) { result =>
                  complete(result.get.asInstanceOf[SCPPMessage])
                }
              }
            }
          } ~
          get {
            logRequest("GET-ALL") {
              respondWithHeaders(`Content-Disposition`(attachment, Map("filename" -> "report.csv"))) {
                onComplete((scppStoreActor ? GetAllCommand).mapTo[GetResult]) { result =>
                  val response: Array[Byte] = createCSV(result.get)
                  complete(HttpResponse(entity = HttpEntity(ContentType(`text/csv`, `UTF-8`), response)))
                }
              }
            }
          }
        }
      } ~
      pathPrefix("tariff") {
        pathEnd {
          post {
            entity(as[Tariff]) { data =>
              logRequest("STORE-TARIFF") {
                onComplete(scppStoreActor ? AddTariffCommand(data)) { result =>
                  complete(result.map(_ => HttpResponse(Created)).get)
                }
              }
            }
          }
        }
      } ~
      path("invoices" / IntNumber / IntNumber / """(.+).txt""".r).as(Invoice) { invoice =>
        pathEnd {
          get {
            logRequest(s"GET-INVOICE for ${invoice.name} ${invoice.year} ${invoice.monthName}") {
              onComplete((scppStoreActor ? GetCommand(invoice.name, invoice.year, invoice.month)).mapTo[GetResult]) { result =>
                complete {
                  txt.invoice.render(invoice.name, invoice.monthName, invoice.year.toString, result.get.items)
                }
              }
            }
          }
        }
      }
    }

  private def createCSV(result: GetResult): Array[Byte] = {
    val baos = new ByteArrayOutputStream()
    val w = CSVWriter.open(new java.io.OutputStreamWriter(baos, "UTF-8"))
    result.items.foreach(message =>
      w.writeRow(Seq(message.customerId,
        ISODateTimeFormat.dateTimeNoMillis.print(message.startTime),
        ISODateTimeFormat.dateTimeNoMillis.print(message.endTime),
        message.volume,
        message.fee.getOrElse("-"))))
    baos.toByteArray
  }

  /** Helper function to convert [[ErrorResponse]] to [[HttpResponse]]
    *
    * @param status status code
    * @param error  error message
    * @return error representation
    */
  def convertError(status: StatusCode, error: ErrorResponse) = {
    onSuccess(Marshal(error).to[ResponseEntity]) { entity =>
      complete {
        HttpResponse(status = status, entity = entity)
      }
    }
  }

  /** Class to validate input parameters for invoice reques
    *
    * @param year year
    * @param month month number
    * @param name cultomer id
    */
  case class Invoice(year: Int, month: Int, name: String) {
    require(!name.isEmpty, "customer name must not be empty")
    require(1990 <= year && year <= clock.getCurrentDateTime.year().get(),
      "year must be between 1990 and current year")
    require(1 <= month && month <= 12, "month must be between 1 and 12")

    /** Convert month number into word
      *
      * @return string month name
      */
    def monthName: String =
      YearMonth.of(year, month).getMonth.getDisplayName(TextStyle.FULL, Locale.getDefault)
  }
}

/** Class represents error response converted to Json
  *
  * @param error error message
  */
case class ErrorResponse(error: String)

object Main extends App {

  implicit val system = ActorSystem("scpp-system")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  val host = if (args.length > 0 && args(0) != null) args(0) else "localhost"
  val port = if (args.length > 1 && args(1) != null) args(1).toInt else 8080

  val bindingFuture = Http().bindAndHandle(
    new SCPPRoute(system.actorOf(SCPPStoreActor.prop(FeeCalculator))).route, host, port)

  println(s"SCPP server started on interface $host and port $port")
  println("Press any key to exit...")

  StdIn.readLine()

  bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
}

