package web

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.{Marshaller, _}
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.{ContentType, HttpEntity}
import core._
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.{DateTime, DateTimeZone}
import spray.json.DefaultJsonProtocol

import scala.util.Try

/**
  * Trait to support serialization to JSON format
  */
trait SCPPProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  import spray.json._

  implicit object DateTimeFormat extends RootJsonFormat[DateTime] {

    val parser = ISODateTimeFormat.dateOptionalTimeParser()

    def write(obj: DateTime): JsValue = {
      JsString(ISODateTimeFormat.dateTimeNoMillis.print(obj))
    }

    def read(json: JsValue): DateTime = json match {
      case JsString(s) => Try(parser.parseDateTime(s).withZone(DateTimeZone.UTC)).getOrElse(error(s))
      case _ => error(json.toString())
    }

    def error(v: Any): DateTime = {
      deserializationError(s"""'$v' is not a valid date value.""")
    }
  }

  implicit val scppFormat = jsonFormat5(SCPPMessage)

  implicit val tariffFormat = jsonFormat4(Tariff)

  implicit val errorFormat = jsonFormat1(ErrorResponse)

  implicit def errorResponseMarshaller: ToEntityMarshaller[ErrorResponse] =
    Marshaller.withFixedContentType(`application/json`) { error =>
      HttpEntity(ContentType(`application/json`), error.toJson.compactPrint)
    }
}
