package web

import akka.actor.Status.Failure
import akka.actor.{ActorRef, ActorSystem, Status}
import akka.testkit.{ImplicitSender, TestKit}
import core.{FeeCalculator, NoSuitableTariffFound, SCPPMessage, Tariff}
import org.joda.time.DateTime
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class SCPPStoreActorTest(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  def this() = this(ActorSystem("MySpec"))
  val clock = mock[Clock]
  val feeCalculator = mock[FeeCalculator]

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "SCPPActor actor" must {

    "successfully get all messages when empty" in {
      val scppActor = createActor
      getAll(scppActor, Seq[SCPPMessage]())
    }

    "successfully send message and get messages" in {
      val scppActor = createActor
      val message = SCPPMessage("customerId", DateTime.now(), DateTime.now(), 100)
      (feeCalculator.calculate _).expects(message, Seq[Tariff]()).once().returns(message)
      store(scppActor, message)
      getAll(scppActor, Seq(message))
    }

    "fail to send message without suitable tariff" in {
      val scppActor = createActor
      val message = SCPPMessage("customerId", DateTime.now(), DateTime.now(), 100)
      (feeCalculator.calculate _).expects(message, Seq[Tariff]()).once().throws(NoSuitableTariffFound)
      scppActor ! StoreCommand(message)
      expectMsg(Failure(NoSuitableTariffFound))
    }

    "successfully send several messages and get messages" in {
      val now = DateTime.now()
      val scppActor = createActor
      val seq = Seq(
        SCPPMessage("customerId", now, now, 100),
        SCPPMessage("customerId", now, now, 200),
        SCPPMessage("customerId", now, now, 300),
        SCPPMessage("customerId", now, now, 400),
        SCPPMessage("customerId", now, now, 500)
      )
      seq.foreach(message => {
        (feeCalculator.calculate _).expects(message, Seq[Tariff]()).once().returns(message)
        store(scppActor, message)
      })
      getAll(scppActor, seq)
    }

    "successfully add single tariff" in {
      val now = DateTime.now()
      val scppActor = createActor
      val tariff = Tariff(1, 2, 3, now.plusDays(1))
      (clock.getCurrentDateTime _).expects().once().returns(now)
      addTariff(scppActor, tariff)
    }

    "reject adding single tariff in past" in {
      val now = DateTime.now()
      val scppActor = createActor
      val tariff = Tariff(1, 2, 3, now.minusDays(1))
      (clock.getCurrentDateTime _).expects().once().returns(now)
      failToAddTariff(scppActor, tariff)
    }

    "reject adding single tariff before previous" in {
      val now = DateTime.now()
      val scppActor = createActor
      val tariff1 = Tariff(1, 2, 3, now.plusDays(2))
      (clock.getCurrentDateTime _).expects().twice().returns(now)
      addTariff(scppActor, tariff1)
      val tariff2 = Tariff(1, 2, 3, now.plusDays(1))
      failToAddTariff(scppActor, tariff2)
    }

    "reject adding same tariff twice" in {
      val now = DateTime.now()
      val scppActor = createActor
      val tariff = Tariff(1, 2, 3, now.plusDays(2))
      (clock.getCurrentDateTime _).expects().twice().returns(now)
      addTariff(scppActor, tariff)
      failToAddTariff(scppActor, tariff)
    }

    "reject adding tariff between previous and now" in {
      val t1 = DateTime.now()
      val scppActor = createActor
      val tariff1 = Tariff(1, 2, 3, t1.plusDays(1))
      (clock.getCurrentDateTime _).expects().once().returns(t1)
      addTariff(scppActor, tariff1)

      val t2 = DateTime.now().plusMonths(1)
      (clock.getCurrentDateTime _).expects().once().returns(t1)
      val tariff2 = Tariff(1, 2, 3, t1.minusDays(1))
      failToAddTariff(scppActor, tariff2)
    }

    "get messages for customer" in {
      val scppActor = createActor
      val seq = Seq(
        SCPPMessage("customerId", DateTime.parse("2014-10-27T13:32:14Z"), DateTime.parse("2014-10-27T13:32:14Z"), 100),
        SCPPMessage("customerId", DateTime.parse("2014-10-01T13:32:14Z"), DateTime.parse("2014-11-27T13:32:14Z"), 200),
        SCPPMessage("customerId", DateTime.parse("2014-06-27T13:32:14Z"), DateTime.parse("2014-07-27T13:32:14Z"), 300),
        SCPPMessage("customerId", DateTime.parse("2015-01-27T13:32:14Z"), DateTime.parse("2015-01-27T13:32:14Z"), 400),
        SCPPMessage("john", DateTime.parse("2014-10-27T13:32:14Z"), DateTime.parse("2014-10-27T13:32:14Z"), 500)
      )
      seq.foreach(message => {
        (feeCalculator.calculate _).expects(message, Seq[Tariff]()).once().returns(message)
        store(scppActor, message)
      })
      get(scppActor, "customerId", 10, 2014, 2)
      get(scppActor, "customerId", 10, 2015, 0)
      get(scppActor, "john", 10, 2014, 1)
      get(scppActor, "mary", 10, 2014, 0)
    }
  }

  def createActor: ActorRef = {
    system.actorOf(SCPPStoreActor.prop(feeCalculator, clock))
  }

  def failToAddTariff(scppActor: ActorRef, tariff: Tariff): Unit = {
    scppActor ! AddTariffCommand(tariff)
    expectMsg(Failure(WrongTariffStartDate))
  }

  private def addTariff(scppActor: ActorRef, tariff: Tariff): Unit = {
    scppActor ! AddTariffCommand(tariff)
    expectMsgType[Status.Success]
  }

  private def store(scppActor: ActorRef, message: SCPPMessage): Unit = {
    scppActor ! StoreCommand(message)
    expectMsgType[Status.Success]
  }

  private def getAll(scppActor: ActorRef, result: Seq[SCPPMessage]): Unit = {
    scppActor ! GetAllCommand
    expectMsg(GetResult(result))
  }

  private def get(scppActor: ActorRef, customer: String, month: Int, year: Int, size: Int): Unit = {
    scppActor ! GetCommand(customer, year, month)
    val result = expectMsgType[GetResult]
    assert(result.items.size == size)
  }
}
