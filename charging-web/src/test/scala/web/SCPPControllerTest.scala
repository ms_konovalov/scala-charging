package web

import akka.actor.{Actor, Props, Status}
import akka.http.scaladsl.model.HttpCharsets._
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{ContentType, HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.TestActorRef
import core._
import org.joda.time.DateTime
import org.joda.time.DateTimeZone._
import org.scalamock.scalatest.MockFactory
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class SCPPControllerTest extends WordSpec with Matchers with ScalatestRouteTest with SCPPProtocol
  with MockFactory with TableDrivenPropertyChecks {

  val feeCalculator = mock[FeeCalculator]
  val probe = TestActorRef(Props(new SCPPStoreActor(feeCalculator)))
  val route = new SCPPRoute(probe).route

  "The service" should {

    "return a NotFound error for GET requests to the root path" in {
      Get() ~> Route.seal(route) ~> check {
        status shouldEqual NotFound
      }
    }

    val results = Table(
      "value",
      Seq[SCPPMessage](SCPPMessage("customerId", DateTime.now(), DateTime.now(), 100)),
      Seq[SCPPMessage](SCPPMessage("customerId", DateTime.now(), DateTime.now(), 100),
        SCPPMessage("customerId", DateTime.now(), DateTime.now(), 100),
        SCPPMessage("customerId", DateTime.now(), DateTime.now(), 100)
      )
    )

    "return Success for GET requests to the /scpp" in {
      forAll(results) { value =>
        val route = new SCPPRoute(createMockActor(GetResult(value))).route
        Get("/scpp") ~> route ~> check {
          status shouldEqual OK
          responseEntity.contentType shouldEqual ContentType(`text/csv`, `UTF-8`)
        }
      }
    }

    "return Created for POST requests to the /tariff with correct Data" in {
      Post("/tariff", Tariff(1, 2, 3, DateTime.now().plusDays(1))) ~> route ~> check {
        status shouldEqual Created
      }
    }

    "return BadRequest for POST requests to the /tariff with incorrect Data" in {
      Post("/tariff", Tariff(1, 2, 3, DateTime.now().minusDays(1))) ~> route ~> check {
        status shouldEqual BadRequest
      }
    }

    "return UnsupportedMediaType for POST requests to the /tariff with text/plain" in {
      Post("/tariff", "some data") ~> Route.seal(route) ~> check {
        status shouldEqual UnsupportedMediaType
      }
    }

    "return BadRequest for POST requests to the /tariff with wrong json" in {
      Post("/tariff", HttpEntity(ContentType(MediaTypes.`application/json`), """{"some" : "data"}""")) ~>
        Route.seal(route) ~> check {
        status shouldEqual BadRequest
      }
    }

    "return InternalServerError for POST requests to the /tariff if something went wrong" in {
      val route = new SCPPRoute(createMockActor(Status.Failure(new RuntimeException()))).route
      Post("/tariff", Tariff(1, 2, 3, DateTime.now().plusDays(1))) ~> route ~> check {
        status shouldEqual InternalServerError
        responseAs[ErrorResponse] shouldEqual ErrorResponse("Operation failed")
      }
    }

    "return OK for POST requests to the /scpp with correct Data" in {
      val time = DateTime.now().withZone(UTC).withMillis(0)
      val message = SCPPMessage("customerId1", time, time, 100)
      (feeCalculator.calculate _).expects(message, *).once().returns(message)
      Post("/scpp", message) ~> route ~> check {
        status shouldEqual OK
      }
    }

    "return BadRequest for POST requests to the /cspp with incorrect state" in {
      val time = DateTime.now().withZone(UTC).withMillis(0)
      val message = SCPPMessage("customerId2", time, time, 100)
      (feeCalculator.calculate _).expects(message, *).once().throws(NoSuitableTariffFound)
      Post("/scpp", message) ~> route ~> check {
        status shouldEqual BadRequest
      }
    }

    "return UnsupportedMediaType for POST requests to the /scpp with text/plain" in {
      Post("/scpp", "some data") ~> Route.seal(route) ~> check {
        status shouldEqual UnsupportedMediaType
      }
    }

    "return BadRequest for POST requests to the /scpp with wrong json" in {
      Post("/scpp", HttpEntity(ContentType(MediaTypes.`application/json`), """{"some" : "data"}""")) ~>
        Route.seal(route) ~> check {
        status shouldEqual BadRequest
      }
    }

    "return InternalServerError for POST requests to the /scpp if something went wrong" in {
      val route = new SCPPRoute(createMockActor(Status.Failure(new RuntimeException()))).route
      Post("/scpp", SCPPMessage("customerId", null, null, 100)) ~> route ~> check {
        status shouldEqual InternalServerError
        responseAs[ErrorResponse] shouldEqual ErrorResponse("Operation failed")
      }
    }

    val results2 = Table(
      ("url", "result"),
      ("/invoices/1000/2/a.txt", BadRequest),
      ("/invoices/2777/13/a.txt", BadRequest),
      ("/invoices/2000/444/a.txt", BadRequest),
      ("/invoices/2000/0/a.txt", BadRequest),
      ("/invoices/2000/2/atxt", NotFound)
    )

    "return BadRequest for GET requests to the /scpp" in {
      forAll(results2) { (url, result) =>
        val route = new SCPPRoute(createMockActor(GetResult(Seq[SCPPMessage]()))).route
        Get(url) ~> Route.seal(route) ~> check {
          status shouldEqual result
        }
      }
    }

    "return OK for GET requests to the /invoices/..." in {
      val route = new SCPPRoute(createMockActor(GetResult(Seq[SCPPMessage]()))).route
      Get("/invoices/1999/2/john.txt") ~> route ~> check {
        status shouldEqual OK
        responseEntity.contentType shouldEqual ContentType(`text/plain`, `UTF-8`)
      }
    }
  }

  private def createMockActor(response: Any) = {
    TestActorRef(new Actor {
      def receive = {
        case _ => sender() ! response
      }
    })
  }

}