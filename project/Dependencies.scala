import sbt._

object Version {
  val scala = "2.11.8"
  val scalaXml = "1.0.4"
  val scalaReflect = "2.11.8"
  val scalaTest = "2.2.6"
  val scalaMock = "3.2.2"
  val akka = "2.4.2"
  val logback = "1.1.3"
}

object Library {
  val scalaTime = "com.github.nscala-time" %% "nscala-time" % "2.12.0"
  val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest % "it,test"
  val scalaMock = "org.scalamock" %% "scalamock-scalatest-support" % Version.scalaMock % "it,test"
  val akkaHttp = "com.typesafe.akka" %% "akka-http-experimental" % Version.akka
  val akkaHttpTest = "com.typesafe.akka" %% "akka-http-testkit" % Version.akka % "it,test"
  val akkaJson = "com.typesafe.akka" %% "akka-http-spray-json-experimental" % Version.akka
  val json4sJackson = "org.json4s" %% "json4s-jackson" % "3.3.0"
  val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % Version.akka
  val csv = "com.github.tototoshi" %% "scala-csv" % "1.3.3"
  val twirl = "btomala" %% "akka-http-twirl" % "1.1.0"
}

object Dependencies {

  import Library._

  val web = Seq(
    akkaHttp,
    akkaJson,
    json4sJackson,
    akkaHttpTest,
    scalaTest,
    scalaMock,
    akkaSlf4j,
    csv,
    twirl
  )

  val core = Seq(
    scalaTime,
    scalaTest,
    scalaMock
  )

  val overrides = Set(
    "org.scala-lang.modules" %% "scala-xml" % Version.scalaXml,
    "org.scala-lang" % "scala-reflect" % Version.scalaReflect
  )
}