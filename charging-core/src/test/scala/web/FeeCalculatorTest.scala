package web

import core.{FeeCalculator, NoSuitableTariffFound, SCPPMessage, Tariff}
import org.joda.time.{DateTime, DateTimeUtils}
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers}

class FeeCalculatorTest extends FreeSpec with Matchers with TableDrivenPropertyChecks {

  val calc = FeeCalculator
  val now = DateTime.now()

  "FeeCalculator" - {

    val failCases = Table(
      ( "tariffs",
        "message"),
      ( Seq[Tariff](),
        SCPPMessage("", now, now, 1)),
      ( Seq(Tariff(0, 0, 0, now.plusDays(100))),
        SCPPMessage("", now, now.plusDays(20), 1)),
      ( Seq(Tariff(0, 0, 0, now.plusDays(100)),
            Tariff(0, 0, 0, now.plusDays(200)),
            Tariff(0, 0, 0, now.plusDays(300))),
        SCPPMessage("", now, now.plusDays(20), 1))
    )

    "should throw NoSuitableTariffFound" - {
      forAll(failCases) { (tariffs, message) =>
        intercept[NoSuitableTariffFound.type] {
          calc.calculate(message, tariffs)
        }
      }
    }

    val okCases = Table(
      ( "tariffs",
        "message",
        "result"),
      ( Seq(Tariff(0, 0, 0, now.minusDays(100))),
        SCPPMessage("", now, now.plusDays(20), 1),
        0),
      ( Seq(Tariff(0, 0, 0, now.plusDays(100)),
            Tariff(0, 0, 0, now.plusDays(200)),
            Tariff(1, 1, 1, now.plusDays(300))),
        SCPPMessage("", now.plusDays(500), now.plusDays(500).plusHours(1), 1),
        3),
      ( Seq(Tariff(0, 0, 0, now),
            Tariff(0, 0, 0, now.plusDays(20)),
            Tariff(10, 5, 1, now.plusDays(30)),
            Tariff(5, 5, 5, now.plusDays(100))),
        SCPPMessage("", now.plusDays(50), now.plusDays(50).plusHours(10), 36),
        10 + 5 * 10 + 1 * 36),
      ( Seq(Tariff(0.20, 1.00, 0.25, DateTime.parse("2013-01-01T00:00:00Z")),
            Tariff(1.50, 0.50, 0.30, DateTime.parse("2014-10-28T00:00:00Z"))),
        SCPPMessage("pete", DateTime.parse("2014-10-27T13:32:14Z"), DateTime.parse("2014-10-27T14:32:14Z"), 13.21),
        4.50),
      ( Seq(Tariff(0.20, 1.00, 0.25, DateTime.parse("2013-01-01T00:00:00Z")),
            Tariff(1.50, 0.50, 0.30, DateTime.parse("2014-10-28T00:00:00Z"))),
        SCPPMessage("john", DateTime.parse("2014-10-28T09:34:17Z"), DateTime.parse("2014-10-28T16:45:13Z"), 32.03),
        14.70)
    )

    "should calculate" - {
      forAll(okCases) { (tariffs, message, result) =>
        val r = calc.calculate(message, tariffs)
        assert(r.fee.get == result.asInstanceOf[Number])
      }
    }
  }
}
