package core

import org.joda.time.{DateTime, Seconds}

import scala.math.BigDecimal.RoundingMode

/** Class represents SCPP message from charging station
  *
  * @param customerId customer id
  * @param startTime start time of charging
  * @param endTime end time of charging
  * @param volume amount of electricity consumed in kWh
  * @param fee calculated fee, [[None]] when message is coming from station
  */
case class SCPPMessage(customerId: String,
                       startTime: DateTime,
                       endTime: DateTime,
                       volume: BigDecimal,
                       fee: Option[BigDecimal] = None)

/** Class represents Tariff uploaded by operator
  *
  * @param startFee fee for starting charging process
  * @param hourlyFee fee per hour for parking lot
  * @param feePerKWh fee for energy per kWh
  * @param activeStarting time when this tariff starts to be actual
  */
case class Tariff(startFee: BigDecimal,
                  hourlyFee: BigDecimal,
                  feePerKWh: BigDecimal,
                  activeStarting: DateTime)

/** Interface for fee calculator */
trait FeeCalculator {

  /** Calculate method
    *
    * @param message SCPP message about charging ttransaction
    * @param tariffs all available tariffs
    * @throws NoSuitableTariffFound if there are no suitable tariffs
    * @return [[SCPPMessage]] with filled [[SCPPMessage.fee]] field by founded actual tariff
    */
  @throws[NoSuitableTariffFound.type]
  def calculate(message: SCPPMessage, tariffs: Seq[Tariff]): SCPPMessage

}

/** Default implementation for [[FeeCalculator]] trait */
object FeeCalculator extends FeeCalculator {

  /** Calculate method
    *
    * @param message SCPP message about charging ttransaction
    * @param tariffs all available tariffs
    * @throws NoSuitableTariffFound if there are no any suitable tariff with [[Tariff.activeStarting]] less then or equal to start time of charging transaction
    * @return [[SCPPMessage]] with filled [[SCPPMessage.fee]] field by founded actual tariff
    */
  def calculate(message: SCPPMessage, tariffs: Seq[Tariff]): SCPPMessage = {
    findTariff(message, tariffs).map(t =>
      SCPPMessage(message.customerId, message.startTime, message.endTime, message.volume, Some(applyTariff(message, t)))).getOrElse {
      throw NoSuitableTariffFound
    }
  }

  /** Method finds suitable tariff. Only one tariff can be used so if new tariff becomes active during charging transaction,
    * all transaction will be calculated with old tariff */
  private def findTariff(message: SCPPMessage, tariffs: Seq[Tariff]): Option[Tariff] =
    tariffs.filter(t => t.activeStarting.isBefore(message.startTime)
      || t.activeStarting.isEqual(message.startTime)).lastOption

  private def applyTariff(message: SCPPMessage, tariff: Tariff) = {
    (tariff.startFee + tariff.hourlyFee * Seconds.secondsBetween(message.startTime, message.endTime).getSeconds / 3600.0 +
      tariff.feePerKWh * message.volume).setScale(2, RoundingMode.HALF_DOWN)
  }
}

object NoSuitableTariffFound extends Exception
