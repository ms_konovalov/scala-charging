# The New Motion Programming Assignment

## Outline

The Venerable Inertia (TVI) is a company that makes money from charging transactions of electric vehicles on their charge points. In this assignment, you will implement a simple back-office system for them that computes how much money electric drivers owe TVI for their charging transactions. The system will receive messages from charge points that tell the system how much each user charged. When a TVI employee accesses the system, the system will give him or her an overview of how much each customer has to pay.

## User stories

At The New Motion we are working using Agile methodologies, so the software functionality we need is broken down into *user stories* that explain how the software should behave. This assignment will also consist of three such user stories.

### Story 1: Listen for SCPP messages and store transaction information

As a The Venerable Inertia bookkeeper, I want to see an overview of when people charged and where so that I can send invoices to them based on this information.

The charge points speak a very simple charge point communication protocol known as Simple Charge Point Protocol (SCPP). In SCPP, there is only one kind of message: the Transaction message. A Transaction message is a piece of data in the [JSON](http://json.org/) format. The SCPP message is a JSON object with at least the following four fields:

<table>
  <tr>
	<th>Field name</th>
	<th>Type</th>
	<th>Description</th>
	<th>Example value</th>
  </tr>
  <tr>
	<td>customerId</td>
	<td>string</td>
	<td>A string identifying the customer who charged his car in this transaction</td>
	<td>"pete"</td>
  </tr>
  <tr>
	<td>startTime</td>
	<td>string</td>
	<td>The time the transaction started, in ISO8601 format with "Z" as the timezone specification</td>
	<td>"2014-10-27T13:32:14Z"</td>
  </tr>
  <tr>
	<td>endTime</td>
	<td>string</td>
	<td>The time the transaction ended, in ISO8601 format with "Z" as the timezone specification</td>
	<td>"2014-10-27T14:32:14Z"</td>
  </tr>
  <tr>
	<td>volume</td>
	<td>number</td>
	<td>The amount of energy consumed in kWh</td>
	<td>13.21</td>
  </tr>
</table>

An example of a full SCPP message would thus be:

```
{
  "customerId": "john",
  "startTime": "2014-10-28T09:34:17Z",
  "endTime": "2014-10-28T16:45:13Z",
  "volume": 32.03
}

```

The SCPP messages will be delivered to the backoffice with HTTP POST requests to an endpoint URL that you are free to choose. A Python script that can emulate SCPP charge points is provided to you with this assignment. It can be invoked as follows:<br>
`python scpp.py <endpoint URL>`<br>
so for example:<br>
`python scpp.py http://localhost:8080/scpp/`<br>
The script has to be run with a Python 2 interpreter. Such an interpreter is usually installed by default as the `python` command on Apple and Linux systems. If you do not have a Python 2 interpreter, you can get one from [the Python website](https://www.python.org/) (choose the button labeled "Download Python 2.X.X").

The bookkeeper would like to get the overview as a CSV file that he can request from a web server. The CSV file should have four columns: the customer ID, the transaction start time, the transaction end time, and the transaction volume. An example of such a CSV file would be:

```
pete,2014-10-27T13:32:14Z,2014-10-27T14:32:14Z,13.21
john,2014-10-28T09:34:17Z,2014-10-28T16:45:13Z,32.03
```

### Story 2: Compute transaction fees

As a TVI bookkeeper I want the back-office system to compute transaction fees so that I can create invoices much quicker.

A user of course has to pay for charging, to pay for the electricity, the parking spot and to compensate The Venerable Inertia for their work. The transaction tariff consists of three components: a start fee to pay for The Venerable Inertia's processing, a parking fee to pay for the parking spot and a kWh fee for the energy consumed.

The bookkeeper want to set the tariff by sending a POST request to the back-office with a JSON message like this:

```
{
  "startFee": 0.20,
  "hourlyFee": 1.00,
  "feePerKWh": 0.25,
  "activeStarting": "2014-10-28T06:00:00Z"
}
```

The fields have the following meaning:

<table>
  <tr>
	<th>Field name</th>
	<th>Type</th>
	<th>Description</th>
  </tr>
	<td>startFee</td>
	<td>number</td>
	<td>A fee that applies one time to every transaction, expressed in euros</td>
  </tr>
  <tr>
	<td>hourlyFee</td>
	<td>number</td>
	<td>A fee for the time that the car occupied the charger, expressed in euros per hour</td>
  </tr>
  <tr>
	<td>feePerKWh</td>
	<td>number</td>
	<td>A fee for the energy that the car consumed, expressed in euros per kWh</td>
  </tr>
  <tr>
	<td>activeStarting</td>
	<td>string</td>
	<td>The moment from which this tariff is active,  in ISO8601 format with "Z" as the timezone specification</td>
  </tr>
</table>

Your app should listen for POSTs of this format on a certain URL. Your back-office system should check that when a tariff message is POSTed, the activeStarting value is later than the current time, because we cannot retroactively change the charging tariff. For the same reason, the activeStarting value should also be later than the latest activeStarting value in a previous tariff message.

When the bookkeeper requests the overview of charge sessions, your back-office system should include the total session fee as a fifth column. The total session fee should be computed according to the tariff with the last activeStarting value that is before the startTime of the session.

To compute the total fee of a session, you add the three parts: the hourly fee multiplied by the session duration in hours, the fee per kWh multiplied by the consumed energy in kWh, and the start fee.

#### Example

Say the bookkeeper posts two tariff messages, first this one:

```
{
  "startFee": 0.20,
  "hourlyFee": 1.00,
  "feePerKWh": 0.25,
  "activeStarting": "2013-01-01T00:00:00Z"
}
```

and then this one:

```
{
  "startFee": 1.50,
  "hourlyFee": 0.50,
  "feePerKWh": 0.30,
  "activeStarting": "2014-10-28T00:00:00Z"
}
```

And then the bookkeeper requests the session overview containing the two example sessions from Story 1. He should see:

```
pete,2014-10-27T13:32:14Z,2014-10-27T14:32:14Z,13.21,4.50
john,2014-10-28T09:34:17Z,2014-10-28T16:45:13Z,32.03,14.70
```

As an example of the session fee computation, the € 14.70 fee for the second session breaks down as follows:

<table>
  <tr>
	<th>Component</th>
	<th>Component fee</th>
  </tr>
  <tr>
	<td>start fee</td>
	<td>€ 1.50</td>
  </tr>
  <tr>
    <td>hourly fee</td>
    <td>7.18 hours * 0.50 = € 3.59</td>
  </tr>
  <tr>
	<td>kWh fee</td>
	<td>32.03 kWh * 0.30 = € 9.61</td>
  </tr>
  <tr>
	<td>total</td>
	<td>€ 14.70</td>
  </tr>
</table>


### Story 3: Compute invoice amounts

Now that our bookkeeper has the transaction fees computed automatically for him, he is imagining even more automation to make his life easier. He now came up with the following request:

As a TVI bookkeeper, I want to be able to surf to a webpage on<br> `<backoffice hostname and port>/invoices/<year>/<month number>/<customer name>.txt` and see the monthly invoice for the customer with that name in order to generate invoices quickly.

A monthly invoice should contain all the charge sessions of the requested user that have an end time in the requested month.

The invoice should be presented as a text file according to this template:

```
Dear <customer name>,

In <month name> <year>, you have charged:

from <start time> to <end time>: <energy volume> kWh @ <transaction fee>
from <start time> to <end time>: <energy volume> kWh @ <transaction fee>
...

Total amount: <sum of all transaction fees>

Kind regards,
Your dearest mobility provider,
The Venerable Inertia
```

An example would be:

```
Dear pete,

In October 2014, you have charged:

from 2014-10-15 09:00 to 2014-10-15 17:16: 4.17 kWh @ € 3.21
from 2014-10-27 13:32 to 2014-10-27 14:32: 13.21 kWh @ € 4.50
from 2014-10-30 14:23 to 2014-10-30 15:17: 1.5 kWh @ € 1.20

Total amount: € 8.91

Kind regards,
Your dearest mobility provider,
The Venerable Inertia
```


## Story 4: Document

Your back-office system is now making the lives of TVI employees seriously easier. They will however have to use your system, so please supply a document in which you highlight the relevant implementation-dependent information the TVI staff needs to use the system. It has to include at least:

 * How we can run the application
 * The SCPP endpoint URL that the charge points should connect to
 * The URL that tariff messages should be POSTed to

# Solution

This service is written on scala, akka, akka-http. It stores all data in memory and doesn't use any persistence storage.
It provides several HTTP endpoints to operate all data.

#### POST http://{host}:{port}/tariff

This endpoint is supposed to be used for setting tariffs via HTTP POST method in JSON format listed above. You cannot submit tariff that starts acting earlier than current time or earlier than tariffs already submitted. In these cases you will get HTTP 400 BadReques error code. In case all data is valid service will respond with HTTP 201 Created without JSON-body.

#### POST http://{host}:{port}/scpp

This endpoint is supposed to be used for sending SCPP messages and calculate the fee in real time. You have to send SCPP message via POST request in JSON format listed above. Right now fee calculation happens synchronously so it is mandatory for suitable tariff to be imported into the service. Recieving a new SCPP message system tries to find actual tariff - the newest tariff that became active before the start time of current charging session.
If system cannot find actual tariff it responds with HTTP 400 BadRequest error code. Otherwise it responds with HTTP 200 OK and SCPP message in JSON format with calculated fee.

#### GET http://{host}:{port}/scpp

This endpoint is supposed to be used to download full list of SCPP messages for all customers with calculated fee in CSV format. System responds with HTTP 200 OK and Content-Type "text/csv" and Content-Disposition="attachment" so this file will be automatically downloaded by web-browser.

#### GET http://{host}:{port}/invoices/{year}/{month}/{customer}.txt

This endpoint is supposed to be used to generate invoice in text format listed above for customer for 1 month. It is possible to set year between 1990 and current year and set month as number from 1 to 12. Otherwise you will receive HTTP 400 BadRequest error. If URL combined correctly system responds with invoice as text document without Content-Disposition header so it can be opened by web-browser without downloading as a file.
If you put such input parameters as there are no SCPP messages inside the system conforming to them you will not receive any error but receive just empty invoice without transactions.

## Buikding and starting service

To compile and start application go to root directory and

    % sbt package
      [info] Loading project definition from /.../charging/project
      [info] Set current project to charging (in build file:/.../charging/)
      ...
      [info] Packaging /.../charging/charging-web/target/scala-2.11/charging-web_2.11-1.0.jar ...
      [info] Done packaging.
      [success] Total time: 1 s, completed Jul 13, 2016 5:07:41 AM


    % sbt 'project charging-web' "run 127.0.0.1 8899"
      [info] Loading project definition from /media/kms/Data/projects/charging/project
      [info] Set current project to charging (in build file:/media/kms/Data/projects/charging/)
      [info] Set current project to charging-web (in build file:/media/kms/Data/projects/charging/)
      [info] Running web.Main 127.0.0.1 8899
      SCPP server started on interface 127.0.0.1 and port 8899
      Press any key to exit...

### Integration testing ###

Project contains integration test which can be started manually with ScalaTest or via SBT and run on started server (and only with fresh server without stored data)

    % sbt it:test

Default values

    host = localhost
    port = 8080

can be set with

    -Dit.host=<host> -Dit.port=<port>

