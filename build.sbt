resolvers in ThisBuild ++= Seq(
  Resolver.bintrayRepo("btomala", "maven")
)

val commonSettings = Seq(
  version := "1.0",
  scalaVersion := "2.11.8"
)

lazy val root = (project in file("."))
  .settings(
    name := """charging""",
    commonSettings
  ).aggregate(`charging-core`, `charging-web`)

lazy val `charging-core` = (project in file("charging-core"))
  .configs(IntegrationTest)
  .settings(
    name := "charging-core",
    libraryDependencies ++= Dependencies.core,
    dependencyOverrides ++= Dependencies.overrides,
    parallelExecution in Test := false,
    Testing.settings,
    commonSettings
  )

lazy val `charging-web` = (project in file("charging-web"))
  .configs(IntegrationTest)
  .settings(
    name := "charging-web",
    libraryDependencies ++= Dependencies.web,
    dependencyOverrides ++= Dependencies.overrides,
    parallelExecution in Test := false,
    Testing.settings,
    commonSettings
  ).dependsOn(`charging-core`)
  .enablePlugins(SbtTwirl)

mainClass in (Compile, run) := Some("web.Main")

sourceDirectories in (Compile, TwirlKeys.compileTemplates) := (unmanagedSourceDirectories in Compile).value

